# store config location as a variable
$CONFIG_HOME = "$Env:USERPROFILE\AppData\Roaming"
[Environment]::SetEnvironmentVariable('CONFIG_HOME', $CONFIG_HOME, 'User')

# pwsh profile location
New-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders' Personal -Value "$CONFIG_HOME" -Type ExpandString -Force

# default applications
[Environment]::SetEnvironmentVariable('SHELL', "pwsh", 'User')
[Environment]::SetEnvironmentVariable('EDITOR', "hx", 'User')
[Environment]::SetEnvironmentVariable('PAGER', "bat", 'User')
