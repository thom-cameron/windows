#let conf(
  title,
  author,
  date,
  doc,
  paper: "a5", 
  margin: (x: 1.5cm, y: 1.5cm),
) = {
  // document setup
  set document(title: title, author: author)
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
  )
  // headings
  set heading(numbering: "1.1")
  // paragraph
  set text(font: ("Inter"), size: 10pt)
  set par(justify: true)

  // code
  show raw: set text(font: ("CaskaydiaCove NF"), size: 8pt)
  show raw.where(block: true): content => {
    set par(justify: false)
    align(
      center,
      block(
        stroke: 0.5pt,
        inset: 8pt,
        radius: 4pt,
        content,
      ),
    )
  }
  // tables
  set table(align: left, stroke: 0.5pt, inset: 4pt)
  show table: table => [#align(center, table)]
  show table.cell: content => [
    #set text(size: 9pt, hyphenate: false)
    #set par(justify: false)
    #content
  ]
  show table.cell.where(y: 0): strong
  // links
  show link: underline
  // lists
  set list(marker: ("•"), indent: 8pt)
  // references
  set bibliography(style: "nature")

  // title page info
  par(justify: false, text(14pt)[*#title*])
  v(-8pt)
  par(
    justify: false,
    text(10pt)[#author, #date.display("[day padding:none] [month repr:short] [year]")],
  )
  v(8pt)
  outline(
    depth: 2,
    indent: true,
    fill: line(length: 100%, stroke: 0.5pt),
  )
  v(12pt)

  doc
}
