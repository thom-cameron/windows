local function setup()
	-- don't show filename in status bar
	Status.name = function(self)
		return ui.Line({})
	end

	Header.cwd = function(self)
		-- hide header when there's no space
		local max = self._area.w - self._right_width
		if max <= 0 then
			return ui.Span("")
		end

		local h = self._tab.current.hovered
		if not h then
			return ui.Line({})
		end

		-- show symlink details
		local linked = ""
		if h.link_to ~= nil then
			linked = " -> " .. tostring(h.link_to)
		end

		-- show full path in header
		local s = ya.readable_path(tostring(self._tab.current.cwd)) .. "\\" .. h.name .. linked .. self:flags()
		return ui.Span(ya.truncate(s, { max = max, rtl = true })):style(THEME.manager.cwd)
	end
end

return { setup = setup }
