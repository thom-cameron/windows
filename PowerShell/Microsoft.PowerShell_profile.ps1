# prompt
function prompt {
	# insert a newline
	Write-Host

	# detemine the colour to draw the connecting line
	$line_colour = if ($LASTEXITCODE -eq 0) {"White"} else {"Red"}
	
	# add username and host
	Write-Host "$([char]0x256D) " -NoNewLine -Foreground $line_colour

	# add the current folder (abbreviated if home)
	if ( $(Get-Location) -like $Env:USERPROFILE )
	{
		$currentFolder = "~"
	}
	else
	{
		$currentFolder = Split-Path -Path (Get-Location) -Leaf
	}
	Write-Host $currentFolder -Foreground Blue

	Write-Host "$([char]0x2570)" -NoNewLine -Foreground $line_colour	 
	
	return " "
}

# aliases
Set-Alias ls eza -Option AllScope
Set-Alias cat bat -Option AllScope
Set-Alias cd z -Option AllScope

# functions for interactive use
function ll { eza -l }
function la { eza -la }
function tree { eza --tree }
function gf { 
	$target = fzf --walker "dir,hidden" --preview "eza --tree --level 1 --colour=always {}" --height 40%
	if (-not ([string]::IsNullOrEmpty($target))) {
		z $target
	}
}
function hf { 
	Get-Content (Get-PSReadlineOption).HistorySavePath | fzf --height 40% | Set-Clipboard 
}
function pd {
	param ($input_file)
	pandoc -d pdf $input_file -o "(Get_).pdf"
}

# completions
Import-Module PSReadLine
Set-PSReadLineOption -Colors @{ "InlinePrediction" = "DarkGray" }
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete # tab menu
Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward # substring history search
Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward

# variables
$YAZI_FILE_ONE = "C:\Users\$Env:USERNAME\scoop\apps\git\current\usr\bin\file.exe"

# zoxide
Invoke-Expression (& { (zoxide init powershell | Out-String) })
