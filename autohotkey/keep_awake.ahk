#SingleInstance Force

; start the script paused
Pause()

; keep system awake
SetTimer(PressF15, 60000)
PressF15()
{
	Send("{F15}")
}

; toggle with shortcut
!+a::
{
	Pause(-1)
	if !A_IsPaused {
		state_msg := "keeping the system awake"
	} else {
		state_msg := "no longer keeping the system awake"
	}
	MsgBox(state_msg, "keep_awake", 64)
}
