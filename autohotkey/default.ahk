#SingleInstance Force

#Include desktop-switcher.ahk

home := "C:\Users\thomascameron"

; app shortcuts
!+Enter::Run("wt")
!+f::Run("wt yazi " . home)
!+g::Run(home)
!+b::Run('msedge.exe --new-window "about:blank"')
!+n::Run('wt hx "' . home . '\OneDrive - North Ayrshire Council\docs\notes"')
!+c::Run('wt hx "' . home . '\AppData\Roaming"')
!+v::Run("codium")
!+u::#.
!+s::#^v
!+^s::Run("SnippingTool")

; remap keys
!q::!F4
CapsLock::Esc
+CapsLock::CapsLock
Home::#Tab
Launch_App2::Run("wt ipython --quick")

; window management
!j::!+Esc
!k::!Esc
!+j::#Right ; fancyzones window movement
!+k::#Left
!1::GotoDesktopNumber(0)
!2::GotoDesktopNumber(1)
!3::GotoDesktopNumber(2)
!4::GotoDesktopNumber(3)
!5::GotoDesktopNumber(4)
!6::GotoDesktopNumber(5)
!7::GotoDesktopNumber(6)
!8::GotoDesktopNumber(7)
!9::GotoDesktopNumber(8)
!+1::MoveCurrentWindowToDesktop(0)
!+2::MoveCurrentWindowToDesktop(1)
!+3::MoveCurrentWindowToDesktop(2)
!+4::MoveCurrentWindowToDesktop(3)
!+5::MoveCurrentWindowToDesktop(4)
!+6::MoveCurrentWindowToDesktop(5)
!+7::MoveCurrentWindowToDesktop(6)
!+8::MoveCurrentWindowToDesktop(7)
!+9::MoveCurrentWindowToDesktop(8)
PgUp:: GoToPrevDesktop()
PgDn:: GoToNextDesktop()
