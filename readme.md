<div align="center">
    <h1>
        <img src="thwindows/repo_assets/thwindows_logo.png" width="96"><br>
        thom's windows config. files
    </h1>
</div>

![screenshot](thwindows/repo_assets/desktop_screenshot.png)

This repo contains my configuration files for arch windows. See `programs.csv` for a list of packages to install to get everything working.

Overview
--------

This configuration is intended to make Windows behave in a more efficient way, using software which is as minimal and [free](https://www.gnu.org/philosophy/free-sw.en.html) as is practical.

- [autohotkey](https://github.com/AutoHotkey/AutoHotkey) is used to remap keys and add keyboard shortcuts for applications
- Basic tasks like text editing and file management are handled using minimal - usually terminal-based - applications ([helix](https://github.com/helix-editor/helix) and [yazi](https://github.com/sxyazi/yazi) respectively)
- Packages are managed using [winget](https://github.com/microsoft/winget-cli) and [scoop](https://github.com/ScoopInstaller/Scoop)
- Bloated proprietary applications such as Office 365 are used when they are unavoidable (as Windows is in most workplaces)

This repository is designed to track the files in `$Env:UserProfile\AppData\Roaming`. Not all apps look in this folder for configs, but a lot can be pointed to it with environment variables or similar. 
